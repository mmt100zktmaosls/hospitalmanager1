package com.jmountain.hospitalmanager1.service;

import com.jmountain.hospitalmanager1.entity.HospitalCustomer;
import com.jmountain.hospitalmanager1.model.CustomerRequest;
import com.jmountain.hospitalmanager1.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(request).build();

        hospitalCustomerRepository.save(addData);
    }
    public HospitalCustomer getData(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }
}