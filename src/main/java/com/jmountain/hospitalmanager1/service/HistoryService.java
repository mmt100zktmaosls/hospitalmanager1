package com.jmountain.hospitalmanager1.service;

import com.jmountain.hospitalmanager1.entity.ClinicHistory;
import com.jmountain.hospitalmanager1.entity.HospitalCustomer;
import com.jmountain.hospitalmanager1.model.HistoryItem;
import com.jmountain.hospitalmanager1.model.HistoryRequest;
import com.jmountain.hospitalmanager1.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory.ClinicHistoryBuilder(hospitalCustomer, historyRequest).build();

        clinicHistoryRepository.save(addData);
    }
}