package com.jmountain.hospitalmanager1.model;

import com.jmountain.hospitalmanager1.entity.ClinicHistory;
import com.jmountain.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)//다른 패키지나 파일에서 new 값을 생성하지 못하게 막는다.
public class HistoryItem {
    private Long historyId;
    private Long customerId;

    private String customerName;
    private String customerPhone;
    private String registrationNumber;
    private String medicalItemName;
    private Double price;
    private String isSalaryName;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculate;

    private HistoryItem(HistoryItemBuilder builder) { // 히스토리 아이템을 히스토리 빌더 타입으로 지정한다.
        this.historyId = builder.historyId; // 빌더에게 히스토리Id를 받아온다.
        this.customerId = builder.customerId; // this 는 빌더에게 값을 줄 때 쓰는 코드이다.
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.registrationNumber = builder.registrationNumber;
        this.medicalItemName = builder.medicalItemName;
        this.price = builder.price;
        this.isSalaryName = builder.isSalaryName;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;
    }
    public static class HistoryItemBuilder implements CommonModelBuilder<HistoryItem> { //HistoryItem 에 빌더 구현
        //builder 패턴은 @Setter를 대신한다. 히스토리아이템에 대한 빌더를 생성한다.

        private final Long historyId;  //필수항목이기 때문에 final 을 붙인다.
        private final Long customerId;
        private final String customerName;
        private final String customerPhone;
        private final String registrationNumber;
        private final String medicalItemName;
        private final Double price;
        private final String isSalaryName;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final String isCalculate;

        public HistoryItemBuilder(ClinicHistory clinicHistory) { // entity 파일, 원본을 가져와 빌더를 형성한다.
            //원본의 타입은 ClinicHistory 이다.
            this.historyId = clinicHistory.getId(); //클리닉 히스토리에서 Id 를 얻어온다.
            this.customerId = clinicHistory.getHospitalCustomer().getId(); //병원고객 엔티티에서 아이디를 얻어 클리닉 히스토리로 가져온다.
            this.customerName = clinicHistory.getHospitalCustomer().getCustomerName();
            this.customerPhone = clinicHistory.getHospitalCustomer().getCustomerPhone();
            this.registrationNumber = clinicHistory.getHospitalCustomer().getRegistrationNumber();
            this.medicalItemName = clinicHistory.getMedicalItem().getName();
            this.price = clinicHistory.getPrice();
            this.isSalaryName = clinicHistory.getIsSalary() ? "예" : "아니요";
            this.dateCure = clinicHistory.getDateCure();
            this.timeCure = clinicHistory.getTimeCure();
            this.isCalculate =clinicHistory.getIsCalculate() ? "예" : "아니요"; // Boolean 타입이기 때문에 ? "Y" : "N" 으로 코딩한다.
        }

        @Override
        public HistoryItem build() {
            return new HistoryItem(this);
        }
    }
}
