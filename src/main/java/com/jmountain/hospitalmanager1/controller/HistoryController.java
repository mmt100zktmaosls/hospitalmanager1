package com.jmountain.hospitalmanager1.controller;

import com.jmountain.hospitalmanager1.entity.HospitalCustomer;
import com.jmountain.hospitalmanager1.model.HistoryRequest;
import com.jmountain.hospitalmanager1.service.CustomerService;
import com.jmountain.hospitalmanager1.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class HistoryController {

    private final CustomerService customerService;
    private final HistoryService historyService;

    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        HospitalCustomer hospitalCustomer = customerService.getData(customerId);
        historyService.setHistory(hospitalCustomer, request);

        return "OK";
    }
}
