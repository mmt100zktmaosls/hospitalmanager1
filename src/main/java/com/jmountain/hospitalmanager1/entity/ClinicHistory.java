package com.jmountain.hospitalmanager1.entity;

import com.jmountain.hospitalmanager1.enums.MedicalItem;
import com.jmountain.hospitalmanager1.interfaces.CommonModelBuilder;
import com.jmountain.hospitalmanager1.model.HistoryRequest;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class ClinicHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //고유 값 설정.
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalCustomerId", nullable = false)
    private HospitalCustomer hospitalCustomer;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;

    private Double price;
    private Boolean isSalary;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private Boolean isCalculate;

    private ClinicHistory(ClinicHistoryBuilder builder) {
        this.hospitalCustomer = builder.hospitalCustomer;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.isSalary = builder.isSalary;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;
    }
    public static class ClinicHistoryBuilder implements CommonModelBuilder<ClinicHistory> {
        private final HospitalCustomer hospitalCustomer;
        private final MedicalItem medicalItem;
        private final Double price;
        private final Boolean isSalary;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final Boolean isCalculate;

        public ClinicHistoryBuilder(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
            this.hospitalCustomer = hospitalCustomer;
            this.medicalItem = historyRequest.getMedicalItem();
            this.price = historyRequest.getIsSalary() ? historyRequest.getMedicalItem().getSalaryPrice() : historyRequest.getMedicalItem().getNonSalaryPrice();
            this.isSalary = historyRequest.getIsSalary();
            this.dateCure = LocalDate.now();
            this.timeCure = LocalTime.now();
            this.isCalculate = false;
        }

        @Override
        public ClinicHistory build() {
            return new ClinicHistory(this);
        }
    }

}
