package com.jmountain.hospitalmanager1.interfaces;


public interface CommonModelBuilder<T> {
    T build();
}

