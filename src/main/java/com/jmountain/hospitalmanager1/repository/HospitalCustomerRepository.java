package com.jmountain.hospitalmanager1.repository;

import com.jmountain.hospitalmanager1.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}
