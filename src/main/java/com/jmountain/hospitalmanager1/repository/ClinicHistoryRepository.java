package com.jmountain.hospitalmanager1.repository;

import com.jmountain.hospitalmanager1.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {

}
